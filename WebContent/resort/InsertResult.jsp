<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link
	href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@900&display=swap"
	rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link rel="stylesheet" type="text/css" href="style2.css">

<script type="text/javascript" src="script.js"></script>
</head>
<body>
	<header id="header"></header>
	<%
		DecimalFormat df = new DecimalFormat("###,###,###,###");
		String name = request.getParameter("name");
		String adult = request.getParameter("adult");
		String teen = request.getParameter("teen");
		String child = request.getParameter("child");
		String roomNum = request.getParameter("roomNum");
		String date = request.getParameter("date");
		String season = request.getParameter("season");
		int inputAdult = Integer.parseInt(adult.substring(0, 1));
		int inputTeen = Integer.parseInt(teen.substring(0, 1));
		int inputChild = Integer.parseInt(child.substring(0, 1));
		
		int price = 0;
		if(season.equals("비수기")) {
			price = 100000 * inputAdult + 80000 * inputTeen + 50000 * inputChild;
		} else {	
			price = 150000 * inputAdult + 120000 * inputTeen + 70000 * inputChild;
		}
	%>
	<div style="padding-top: 200px; margin: auto;">
	<div style="margin: auto; width: 700px;"><h1 align="center">예약확인</h1>
	<form action="pay.jsp" method="post">
		<table border="1" width="700">
			<tr>
				<td class="book" align="center">방 번호</td>
				<td  style="padding: 20px;"><%=roomNum%></td>
			</tr>
			<tr>
				<td class="book" align="center">날짜</td>
				<td style="padding: 20px;"><%=date%></td>
			</tr>
			<tr>
				<td class="book" align="center">예약자</td>
				<td style="padding: 20px;"><%=name %>님</td>

			</tr>
			<tr>
				<td class="book" align="center">인원</td>
				<td style="padding: 20px;">성인 : <%=adult%>, 청소년 : <%=teen%>, 유아 : <%=child%></td>
			</tr>
			<tr>
				<td class="book" align="center">가격</td>
				<td style="padding: 20px;"><%=df.format(price) %>원</td>
			</tr>
			<tr>
				<td colspan="2" class="book" align="center"><input
					type="submit" value="결제하기" /><input
					type="button" value="취소" onclick="history.back();"/></td>
					
			</tr>
		</table>
		<input type="hidden" name="roomNum" value="<%=roomNum%>"/>
		<input type="hidden" name="date" value="<%=date%>"/>
		<input type="hidden" name="name" value="<%=name%>"/>
		<input type="hidden" name="adult" value="<%=inputAdult%>"/>
		<input type="hidden" name="teen" value="<%=inputTeen%>"/>
		<input type="hidden" name="child" value="<%=inputChild%>"/>
		<input type="hidden" name="price" value="<%=price%>"/>
		</form>
		</div>
	</div>
</body>
</html>