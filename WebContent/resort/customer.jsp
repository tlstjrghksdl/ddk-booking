<%@page import="DTO.RoomDTO"%>
<%@page import="DAO.RoomDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*"%>
<%
	request.setCharacterEncoding("utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link
	href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@900&display=swap"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="style2.css">
<style type="text/css">
div {
	padding: 5px;
}

#outter {
	margin-top: 250px;
	margin-bottom: 350px;
}

.customerService {
	width: 600px;
	margin: auto;
	display: flex;
	flex-direction: column;
}

.customerInform {
	display: flex;
	border: 1px solid;
}

.informDetail {
	width: 100px;
	border-right: 1px solid;
}
</style>
</head>
<body>
	<header id="header"></header>
	<%
		String name = request.getParameter("name");
	String date = request.getParameter("date");
	String room = request.getParameter("room");
	RoomDTO roomDTO = RoomDAO.customerService(date, room, name);
	%>
	<form action="customerUpdate.jsp" method="post">
		<div id="outter">
			<div class="customerService">
				<div class="customerInform">
					<div class="informDetail">이름</div>
					<div><%=roomDTO.getName()%></div>
				</div>

				<div class="customerInform">
					<div class="informDetail">날짜</div>
					<div><%=roomDTO.getDate()%></div>
				</div>

				<div class="customerInform">
					<div class="informDetail">방 번호</div>
					<div><%=roomDTO.getRoomnum()%></div>
				</div>

				<div class="customerInform">
					<div class="informDetail">인원</div>
					<div>
						성인 :
						<%=roomDTO.getAdult()%>, 청소년 :
						<%=roomDTO.getTeen()%>, 유아 :
						<%=roomDTO.getChild()%></div>
				</div>
				<span> <input type="submit" value="수정" /> <input
					type="button" value="삭제"
					onclick="location.href='customerDelete.jsp?name=<%=roomDTO.getName()%>&room=<%=roomDTO.getRoomnum()%>&date=<%=roomDTO.getDate()%>'" />
				</span>
			</div>
		</div>
		<input type="hidden" name="room" value="<%=roomDTO.getRoomnum()%>" />
		<input type="hidden" name="date" value="<%=roomDTO.getDate()%>" /> 
		<input type="hidden" name="name" value="<%=roomDTO.getName()%>" /> 
		<input type="hidden" name="adult" value="<%=roomDTO.getAdult()%>" /> 
		<input type="hidden" name="teen" value="<%=roomDTO.getTeen()%>" /> 
		<input type="hidden" name="child" value="<%=roomDTO.getChild()%>" />

	</form>
	<footer id="footer"></footer>
	<script type="text/javascript" src="script.js"></script>
</body>
</html>