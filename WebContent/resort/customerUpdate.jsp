<%@page import="DAO.RoomDAO"%>
<%@page import="RoomDAO.DAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
div {
	padding: 5px;
}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link
	href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@900&display=swap"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="style2.css">
</head>
<body>
	<%
		String room = request.getParameter("room");
	String date = request.getParameter("date");
	String name = request.getParameter("name");
	if (name == null) {
		name = "";
	}
	String adult = request.getParameter("adult");
	if (adult == null) {
		adult = "";
	}
	String teen = request.getParameter("teen");
	if (teen == null) {
		teen = "";
	}
	String child = request.getParameter("child");
	if (child == null) {
		child = "";
	}
	RoomDAO.customerDelete(name, date, room);
	%>
	<header id="header"></header>
	
	<form action="updateOK.jsp" method="post">
		<div id="outter">
			<div class="customerService">
			<h1>고객 정보 변경</h1>
				<div class="customerInform">
					<div class="informDetail">이름</div>
					<div>
						<input type="text" name="name" value="<%=name%>" />
					</div>
				</div>

				<div class="customerInform">
					<div class="informDetail">날짜</div>
					<div>
						<input type="text" name="date" value="<%=date%>">
					</div>
				</div>

				<div class="customerInform">
					<div class="informDetail">방 번호</div>
					<div>
						<input type="text" name="room" value="<%=room%>">
					</div>
				</div>

				<div class="customerInform">
					<div class="informDetail">인원</div>
					<div>
						<div style="width: 100px;">성인</div><input type="text" name="adult" value="<%=adult%>"><br>
						<div style="width: 100px;">청소년</div><input type="text" name="teen" value="<%=teen%>"><br>
						<div style="width: 100px;">유아</div><input type="text" name="child" value="<%=child%>">
					</div>
				</div>
				<div>
					<input type="submit" value="수정" />
				</div>
			</div>
		</div>
	</form>
	<footer id="footer"></footer>
<script type="text/javascript" src="script.js"></script>
</body>
</html>