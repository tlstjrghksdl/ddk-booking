<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*"%>
<%
	request.setCharacterEncoding("utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link
	href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@900&display=swap"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="style2.css">

</head>
<body>
	<%
		String jump = request.getParameter("jump");
	String id = request.getParameter("id");
	String passwd = request.getParameter("passwd");

	boolean bPassChk = false;

	if (id.replaceAll(" ", "").equals("admin") && passwd.replaceAll(" ", "").equals("admin")) {
		bPassChk = true;
	} else {
		bPassChk = false;
	}

	if (bPassChk) {
		session.setAttribute("login_ok", "yes");
		session.setAttribute("login_id", id);
		System.out.println(session);
		response.sendRedirect(jump);
	} else {
	%>
	<script type="text/javascript">
		alert("아이디 또는 패스워드 오류");
		document.location.href="http://192.168.56.1:8081/Booking/resort/login.jsp?jump=login_test.jsp";
	</script>
	<%
		}
	%>
	<script type="text/javascript" src="script.js"></script>
</body>
</html>