<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="DAO.NotifDAO"%>
<%@page import="DAO.ReviewDAO"%>
<%@page import="DTO.Notif"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.ArrayList"%>

<%
	request.setCharacterEncoding("utf-8");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body>
	<%		String uploadPath = "C:/Users/401-ST05/eclipse-workspace/Booking/WebContent/upload";		// 업로드 경로
			int maxFileSize = 1024 * 1024 * 2;	// 업로드 제한 용량 = 2MB
			String encoding = "utf-8";			// 인코딩
		
			MultipartRequest multi = new MultipartRequest(
				request, 
				uploadPath, 
				maxFileSize, 
				encoding, 
			new DefaultFileRenamePolicy());
			
			// 전 jsp에서 받은 파라미터값을 변수에 담아준다.
			String id =     multi.getParameter("id");
			String title =  multi.getParameter("title");
			String date =   multi.getParameter("date");
			String content= multi.getParameter("content");
			String rootid = multi.getParameter("rootid");
			
			//notice 객체를 생성후 set해준다.
			Notif notice = new Notif();
			notice.setId(id);
			notice.setTitle(title);
			notice.setDate(date);
			notice.setContent(content);
			notice.setRootid(rootid);
			
			//delete메소드를 실행한다.
			ReviewDAO.deleteNotice(Integer.parseInt(notice.getId()));
	%>

	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<script type="text/javascript">
		alert("삭제가 완료됐습니다.")
		location.href="mainReview.jsp";
	</script>
</body>
</html><!-- 
<div style="overflow: auto; width: 500px; height: 500px;"></div> -->