<%@page import="DAO.ReviewDAO"%>
<%@page import="com.mysql.cj.util.DnsSrv.SrvRecord"%>
<%@page import="DTO.UploadFile"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Arrays"%>
<%@page import="DTO.Paging"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="DAO.NotifDAO"%>
<%@ page import="DTO.Notif"%>
<%
	request.setCharacterEncoding("utf-8");
%>


<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<title></title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link
	href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@900&display=swap"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../style2.css">
<script type="text/javascript" src="../script.js"></script>
<!-- 스타일을 미리 정해준다. -->
<script type="text/javascript">
	// 버튼을 클릭했을때 onclick="goSearch()시 mainNotice.jsp로 간다."
	function goSearch() {
		var del = document.forms['inputform'];
		del.action = 'mainNotice.jsp';
		del.submit();
	}
</script>
<style>
table {
	width: 1000px;
	border-top: 1px solid #444444;
	border-bottom: 1px solid #444444;
	border-collapse: collapse;
	margin-bottom: 10px;
}
</style>
</head>
<body>
	<header id="header"></header>
	<div style="margin: auto;">
	<%
		// 공지사항들의 객체를 List에 담는다.
	List<Notif> notices = new ArrayList<>();
	// 검색 조건 (제목, 글쓴이, 내용)
	String searchTitle = request.getParameter("searchTitle");
	// 검색할 내용
	String search = request.getParameter("search");

	// 검색할 내용이 없으면 
	if (search == null || search.trim() == "") {
		// 모두다 출력
		notices = ReviewDAO.selectAll();
	} else {
		//아니면 검색조건만 출력
		notices = ReviewDAO.search(searchTitle, search.trim());
	}

	// 데이터 값이 없다면 UI만 보여준다.
	if (notices.size() == 0) {
	%>
	<div style="margin: auto; height: 100%">
		<div style="padding-top: 100px;">
			<h1>Review</h1>
		</div>
		<form>
			<table>
				<colgroup>
					<col width="5%">
					<col width="60%">
					<col width="15%">
					<col width="10%">
					<col width="10%">
				</colgroup>
				<tr align="center" bgcolor=gray>
					<th>번호</th>
					<th>제목</th>
					<th>등록일</th>
					<th>작성자</th>
					<th>조회수</th>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><a href="mainNotice.jsp"><input type="button"
							id="button" value="글전체" /></a></td>
					<td colspan="5" align="right"><a href="insertNotice.jsp"><input
							type="button" id="button" value="신규" /></a></td>
				</tr>
			</table>
		</form>
	</div>
	<%
		} else {
	// 그게 아닐때, 데이터 수는 notices리스트의 수와 같다.
	int dataCnt = notices.size();
	int lastDataCnt = 0;
	if (dataCnt % 10 == 0) {
		lastDataCnt = 10;
	} else {
		lastDataCnt = dataCnt % 10;
	}

	// Pagenation하기 위해 Paging 클래스를 객체화한다.
	Paging p = new Paging();

	p.setTotalCount(dataCnt);

	// 현재 페이지 받아온 pg의 값이 null이 아니면 그대로 표시하고 null이면 1로 한다 즉 현재페이지 null이면 1페이지
	int cpage = request.getParameter("pg") != null ? Integer.parseInt(request.getParameter("pg")) : 1;
	int prevPage = (int) Math.floor((cpage - 1) / p.getPageSize()) * p.getPageSize();
	int nextPage = prevPage + p.getPageSize() + 1;
	p.setPageNo(cpage);
	p.setPrevPageNo(prevPage);
	p.setNextPageNo(nextPage);

	// url에서 from의 값을 받아서 from에 저장
	String from = request.getParameter("from");
	// url에서 cnt의 값을 받아서 cnt에 저장 
	String cnt = request.getParameter("cnt");

	// url에서 받아온 값을 int로 변환하여 저장할 변수
	int fromPT;
	int cntPT;

	// url에서 from이나 cnt의 값이 없으면 (첫페이지 처리 위함)
	if (from == null || cnt == null) {
		// fromPT=0, cntPT=10인 것으로 판단
		if (dataCnt > 10) {
			fromPT = dataCnt - 10;
			cntPT = 10;
		} else {
			fromPT = 0;
			cntPT = dataCnt;
		}

	} else {
		// 마지막 페이지에서 표시할 데이터 수를 정하기 위한 if문이다. 현재 페이지가 마지막페이지랑 같다면,
		if (cpage == p.getFinalPageNo()) {
			// 데이터수가 꼭 10개가 아니기 때문에 총 데이터에서 10을 나눈 나머지값을 표시할 것이다.
			fromPT = Integer.parseInt(from);
			cntPT = dataCnt % 10;
		}
		fromPT = Integer.parseInt(from);
		cntPT = Integer.parseInt(cnt);
	}
	%>
	<div style="margin: auto; height: 100%">
		<div style="padding-top: 100px;">
			<h1>Notice</h1>
		</div>
		<div>
			<form method="post" name="inputform">

				<table>
					<colgroup>
						<col width="5%">
						<col width="45%">
						<col width="15%">
						<col width="15%">
						<col width="10%">
						<col width="10%">
					</colgroup>
					<tr align="center" bgcolor=gray>
						<th>번호</th>
						<th>제목</th>
						<th>파일</th>
						<th>등록일</th>
						<th>작성자</th>
						<th>조회수</th>
					</tr>
					<%
						// 데이터가 역순이어야하기 때문에 for문을 역순으로 돌렸다. 이건 교안을 안본 내잘못!!
					for (int i = fromPT + cntPT - 1; i >= fromPT; i--) {
					%>
					<tr align="center">
						<td><%=notices.get(i).getId()%></td>
						<td align="left"><a
							href="viewNotice.jsp?id=<%=notices.get(i).getId()%>"><%=notices.get(i).getTitle()%></a></td>
						<td></td>
						<td><%=notices.get(i).getDate()%></td>
						<td><%=notices.get(i).getWriter()%></td>
						<td><%=notices.get(i).getViewcnt()%></td>
					</tr>
					<%
						}
					%>
					<tr>
						<td><a href="mainNotice.jsp"><input type="button"
								id="button" value="글전체" /></a></td>
						<td colspan="5" align="right"><a href="insertNotice.jsp"><input
								type="button" id="button" value="신규" /></a></td>
					</tr>
				</table>


			</form>
			<!-- 페이지 처리 역순-->
			<table id="none">
				<tr>
					<td align="center" style="border-bottom: hidden;">
						<%
							if (p.getNextPageNo() > p.getFinalPageNo()) {
							if (p.getPageNo() / 11 != 0) {
								if (dataCnt % 10 == 0) {
						%> <a
						href="mainNotice.jsp?pg=1&from=<%=10 * (p.getFinalPageNo() - 1)%>&cnt=10"><<</a>
						<a
						href="mainNotice.jsp?pg=<%=p.getPrevPageNo()%>&from=<%=10 * (p.getFinalPageNo() - p.getPrevPageNo()) - (10 - lastDataCnt)%>&cnt=10"><</a>
						<%
							} else {
						%> <a
						href="mainNotice.jsp?pg=1&from=<%=10 * (p.getFinalPageNo() - 1) - (10 - lastDataCnt)%>&cnt=10"><<</a>
						<a
						href="mainNotice.jsp?pg=<%=p.getPrevPageNo()%>&from=<%=10 * (p.getFinalPageNo() - p.getPrevPageNo()) - (10 - lastDataCnt)%>&cnt=10"><</a>
						<%
							}
						}
						for (int i = 1 + p.getPrevPageNo(); i <= p.getFinalPageNo() - 1; i++) {
						%> <a
						href="mainNotice.jsp?pg=<%=i%>&from=<%=10 * (p.getFinalPageNo() - i) - (10 - lastDataCnt)%>&cnt=10">
							<%=i%>
					</a> <%
 	}
 if (dataCnt % 10 == 0) {
 %> <a href="mainNotice.jsp?pg=<%=p.getFinalPageNo()%>&from=0&cnt=10"><%=p.getFinalPageNo()%></a>
						<%
							} else {
						%> <a
						href="mainNotice.jsp?pg=<%=p.getFinalPageNo()%>&from=0&cnt=<%=lastDataCnt%>"><%=p.getFinalPageNo()%></a>
						<%
							}
						} else {
						if (p.getPageNo() / 11 != 0) {
						%> <a href="mainNotice.jsp?pg=1&from=<%=dataCnt - 9%>&cnt=10"><<</a>
						<a
						href="mainNotice.jsp?pg=<%=p.getPrevPageNo()%>&from=<%=p.getPrevPageNo() * 10%>&cnt=10"><</a>
						<%
							}

						for (int i = 1 + p.getPrevPageNo(); i < p.getNextPageNo(); i++) {
						%> <a
						href="mainNotice.jsp?pg=<%=i%>&from=<%=10 * (p.getFinalPageNo() - i) - (10 - lastDataCnt)%>&cnt=10">
							<%=i%>
					</a> <%
 	}
 if (p.getPageNo() != p.getFinalPageNo()) {
 if (p.getNextPageNo() == p.getFinalPageNo()) {
 %> <a
						href="mainNotice.jsp?pg=<%=p.getNextPageNo()%>&from=<%=10 * (p.getFinalPageNo() - p.getNextPageNo()) - (10 - lastDataCnt)%>&cnt=<%=lastDataCnt%>">></a>
						<a
						href="mainNotice.jsp?pg=<%=p.getFinalPageNo()%>&from=0&cnt=<%=lastDataCnt%>">>></a>
						<%} else {%> <a
						href="mainNotice.jsp?pg=<%=p.getNextPageNo()%>&from=<%=10 * (p.getFinalPageNo() - p.getNextPageNo()) - (10 - lastDataCnt)%>&cnt=10">></a>
						<a
						href="mainNotice.jsp?pg=<%=p.getFinalPageNo()%>&from=0&cnt=<%=lastDataCnt%>">>></a>
						<%
							}
						}
						%> <%
 	}
 }
 %>
					</td>
				</tr>
			</table>
			<div style="margin: auto; height: 600px;">
				<form id="inputform">
					<table style="border: 0">
						<tr>
							<td align="center" style="border-bottom: 0"><select
								style="height: 20px" name="searchTitle">
									<option value="검색" selected disabled>==검색==</option>
									<option value="title">제목</option>
									<option value="writer">글쓴이</option>
									<option value="content">내용</option>
							</select> <input type="text" name="search" /> <input type="button"
								onclick="goSearch()" value="검색"
								style="background-color: gray; color: white;" /></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
	</div>
	<footer id="footer"></footer>
</body>
</html>