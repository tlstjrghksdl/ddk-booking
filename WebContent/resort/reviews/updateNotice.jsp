<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="DAO.NotifDAO"%>
<%@page import="DTO.Notif"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.ArrayList"%>

<%
	request.setCharacterEncoding("utf-8");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link
	href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@900&display=swap"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../style2.css">
<script type="text/javascript" src="../script.js"></script>
<script type="text/javascript">

	
	// 뒤로 돌아가기
	function goBack() {
	    window.history.back();
	}

	// 제목의 공백을 잡아주는 함수
	function join(){

		var title = document.fr.title.value;

		var newtitle = title.replace(/(\s*)/g, "");

		if(newtitle.length == 0) {
			alert("공백입니다. 입력해주세요.")
			document.fr.title.focus();
			return false;
		}
	}
		
</script>
<style>
table {
	width: 1000px;
	border-top: 1px solid #444444;
	border-bottom: 1px solid #444444;
	border-collapse: collapse;
	margin-bottom: 10px;
}
</style>
</head>
<body>
<header id="header"></header>
	<%		String uploadPath = "C:/Users/401-ST05/eclipse-workspace/Booking/WebContent/upload";		// 업로드 경로
			int maxFileSize = 1024 * 1024 * 2;	// 업로드 제한 용량 = 2MB
			String encoding = "utf-8";			// 인코딩

			MultipartRequest multi = new MultipartRequest(
				request, 
				uploadPath, 
				maxFileSize, 
				encoding, 
			new DefaultFileRenamePolicy());
			
			// 전 jsp에서 받아온 파라미터값들을 변수에 담는다.
			String id = multi.getParameter("id");
			String writer = multi.getParameter("writer");
			String title = multi.getParameter("title");
			String date = multi.getParameter("date");
			String content = multi.getParameter("content");
			String viewcnt = multi.getParameter("viewcnt");
			String rootid = multi.getParameter("rootid");
			
			// notice객체에 set해준다.
			Notif notice = new Notif();
			notice.setId(id);
			notice.setWriter(writer);
			notice.setTitle(title);
			notice.setDate(date);
			notice.setContent(content);
			notice.setWriter(writer);
			notice.setViewcnt(viewcnt);
			
			// jquery
			request.setAttribute("notice", notice);
	%>
	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<div style="margin: auto; margin-top: 100px;">
	<div><h1>수정</h1></div>
	<form method="post" action="viewInsertedOne.jsp" name="fr" onsubmit="return join()" enctype="multipart/form-data">
		<table border="0">	
		<colgroup>
			<col width="15%">	
			<col width="25%">	
			<col width="15%">	
			<col width="25%">	
			<col width="12%">	
			<col width="8%">	
		</colgroup>
			<tr>
				<th>제목</th>
				<td colspan="5"><input type="text" value="${notice.title}" style="width: 350px;" name="title"></td>
			</tr>
			<tr>
				<th>작성일</th>
				<td><input type="hidden" value="${notice.date}" name="date" />${notice.date}</td>
		
				<th>작성자</th>
				<td><input type="hidden" value="${notice.writer}" name="writer">${notice.writer}</td>
			
				<th>조회수</th>
				<td><input type="hidden" value="${notice.viewcnt}" name="viewcnt">${notice.viewcnt}</td>
			</tr>
			<tr>
				<td colspan="6"><textarea cols="150" rows="16" style="overflow:auto;" name="content">${notice.content}</textarea></td>
			</tr>
			<tr>
				<th>추가 첨부파일</th>
				<td colspan="5"><input type="file" name="upload"/></td>
			</tr>
		</table>
		<input type="hidden" value="<%=rootid %>" name="rootid" />
		<input type="hidden" value="${notice.id}" name="id" />
		<div style="height: 350px">
		<input id="button" style="background-color: gray;" type="button" onclick="goBack();" value="취소"/>
		<input type="submit" id="button" value="수정"/>
		</div>
	</form>
	</div>
	<footer id="footer"></footer>
</body>
</html><!-- 
<div style="overflow: auto; width: 500px; height: 500px;"></div> -->