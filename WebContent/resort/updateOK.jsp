<%@page import="DTO.RoomDTO"%>
<%@page import="DAO.RoomDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

</head>
<body>
	<%
		String room = request.getParameter("room");
	String date = request.getParameter("date");
	String name = request.getParameter("name");
	int adult = Integer.parseInt(request.getParameter("adult"));
	int teen = Integer.parseInt(request.getParameter("teen"));
	int child = Integer.parseInt(request.getParameter("child"));

	RoomDTO dto = new RoomDTO(room, date, name, adult, teen, child);
	RoomDAO.insert(dto);
	%>
	
	<script type="text/javascript">
		alert("수정되었습니다");
		location.href="http://192.168.56.1:8081/Booking/resort/login_test.jsp";
	</script>
</body>
</html>