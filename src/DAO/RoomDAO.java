package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import DTO.RoomDTO;

public class RoomDAO {
	static Connection conn = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	static PreparedStatement pstmt = null;

	public static void connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			String url = "jdbc:mysql://192.168.56.1:33061/resort?"
					+ "serverTimezone=UTC&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&useSSL=false";

			conn = DriverManager.getConnection(url, "root", "kopo36");
			stmt = conn.createStatement();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public static void disconnect() {
		try {
			stmt.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static int roomCheck(String date, int roomNum) {
		int n = 0;
		try {
			rs = stmt.executeQuery(
					"select count(*) from resort where date='" + date + "' and roomnum='" + roomNum + "';");
			if (rs.next()) {
				n = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return n;
	}

	public static void insert(RoomDTO room) {
		connect();
		try {
			pstmt = conn.prepareStatement(
					"insert into resort (roomnum, date, name, adult, teen, child) values(?, ?, ?, ?, ?, ?);");
			pstmt.setString(1, room.getRoomnum());
			pstmt.setString(2, room.getDate());
			pstmt.setString(3, room.getName());
			pstmt.setInt(4, room.getAdult());
			pstmt.setInt(5, room.getTeen());
			pstmt.setInt(6, room.getChild());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
	}

	public static int insertCheck(RoomDTO room) {
		int num = 0;
		connect();
		try {
			rs = stmt.executeQuery("select count(*) from resort where roomnum='" + room.getRoomnum() + "' and date='"
					+ room.getDate() + "' and name='" + room.getName() + "' and adult=" + room.getAdult() + " and teen="
					+ room.getTeen() + " and child=" + room.getChild() + ";");
			if (rs.next()) {
				num = rs.getInt(1);
			} else {
				num = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return num;
	}

	public static RoomDTO getCustomerName(String date, int roomNum) {
		RoomDTO roomDTO = new RoomDTO();
		try {
			rs = stmt.executeQuery("select * from resort where date='" + date + "' and roomnum='" + roomNum + "';");
			if (rs.next()) {
//				System.out.println(rs.getString(1));
//				System.out.println(rs.getString(2));
//				System.out.println(rs.getString(3));
//				System.out.println(rs.getString(4));
//				System.out.println(rs.getString(5));
//				System.out.println(rs.getString(6));
				roomDTO.setRoomnum(rs.getString(2));
				roomDTO.setDate(rs.getString(3));
				roomDTO.setName(rs.getString(4));
				roomDTO.setAdult(rs.getInt(5));
				roomDTO.setTeen(rs.getInt(6));
				roomDTO.setChild(rs.getInt(6));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roomDTO;
	}

	public static RoomDTO customerService(String date, String roomNum, String name) {
		connect();
		RoomDTO roomDTO = new RoomDTO();
		try {
			rs = stmt.executeQuery("select * from resort where date='" + date + "' and roomnum='" + roomNum + "' and name='" + name + "';");
			if (rs.next()) {
//				System.out.println(rs.getString(1));
//				System.out.println(rs.getString(2));
//				System.out.println(rs.getString(3));
//				System.out.println(rs.getString(4));
//				System.out.println(rs.getString(5));
//				System.out.println(rs.getString(6));
				roomDTO.setRoomnum(rs.getString(2));
				roomDTO.setDate(rs.getString(3));
				roomDTO.setName(rs.getString(4));
				roomDTO.setAdult(rs.getInt(5));
				roomDTO.setTeen(rs.getInt(6));
				roomDTO.setChild(rs.getInt(6));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return roomDTO;
	}
	
	public static void customerDelete (String name, String date, String roomNum) {
		connect();
		try {
			pstmt = conn.prepareStatement(
					"delete from resort where name=? and date=? and roomnum=?");
			pstmt.setString(1, name);
			pstmt.setString(2, date);
			pstmt.setString(3, roomNum);
			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}
