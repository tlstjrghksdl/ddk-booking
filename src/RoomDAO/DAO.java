package RoomDAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import RoomDTO.RoomDTO;

public class DAO {
	static Connection conn = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	static PreparedStatement pstmt = null;

	public static void connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			String url = "jdbc:mysql://192.168.23.32:33061/resort?"
					+ "serverTimezone=UTC&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&useSSL=false";

			conn = DriverManager.getConnection(url, "root", "kopo36");
			stmt = conn.createStatement();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public static void disconnect() {
		try { 
			stmt.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static int roomCheck(String date, int roomNum) {
		int n = 0;
		try {
			rs = stmt.executeQuery("select count(*) from resort where date='"+ date +"' and roomnum='"+ roomNum +"';");
			if (rs.next()) {
				n = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return n;
	}
	
	public static void insert(RoomDTO room) {
		connect();
		try {
			pstmt = conn.prepareStatement("insert into resort (roomnum, date, name, adult, teen, child) values(?, ?, ?, ?, ?, ?);");
			pstmt.setString(1, room.getRoomnum());
			pstmt.setString(2, room.getDate());
			pstmt.setString(3, room.getName());
			pstmt.setInt(4, room.getAdult());
			pstmt.setInt(5, room.getTeen());
			pstmt.setInt(6, room.getChild());
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
	}
}
