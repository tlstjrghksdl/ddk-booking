package RoomDTO;


public class RoomDTO {
	String roomnum;
	String date;
	String name;
	int adult;
	int teen;
	int child;

	public String getRoomnum() {
		return roomnum;
	}

	public void setRoomnum(String roomnum) {
		this.roomnum = roomnum;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAdult() {
		return adult;
	}

	public void setAdult(int adult) {
		this.adult = adult;
	}

	public int getTeen() {
		return teen;
	}

	public void setTeen(int teen) {
		this.teen = teen;
	}

	public int getChild() {
		return child;
	}

	public void setChild(int child) {
		this.child = child;
	}

	public RoomDTO(String roomnum, String date, String name, int adult, int teen, int child) {
		super();
		this.roomnum = roomnum;
		this.date = date;
		this.name = name;
		this.adult = adult;
		this.teen = teen;
		this.child = child;
	}

	public RoomDTO() {
		super();
	}

}
